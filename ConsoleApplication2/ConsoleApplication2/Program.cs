﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            var i = 10;
            var j = 20;
            var intList = new List<int>() { 1, 2, 3 };
            DoSomething(out int i, int j, List<int> intList);
            Console.WriteLine("i: {0} j: {1}", i, j);
            Console.Write("intList: ");
            foreach (var ii in intList)
                Console.Write("{0}, ", ii);
        }

      

         public void DoSomething(out int i, int j, List<int> intList)
        {
            i = j;
            intList.Add(intList[0]);
            intList[0] += 10;
            j++;
            i += 10;
        }
    }
}
