﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarbageCollector_Lab9_Exc2
{
    class GarbageCollector
    {
        
        public static void Main(string[] args)
        {
            //GC.Collect()
            Console.WriteLine("***GC.Collect()***");
            AllocateBytes();
            Console.WriteLine("Total Memory: {0}", GC.GetTotalMemory(false));
            GC.Collect();
            Console.WriteLine("Total Memory: {0}", GC.GetTotalMemory(false));
            
            //Collection Mode: Optimized
            Console.WriteLine("----------------");
            Console.WriteLine("Collection Mode: Optimized");
            AllocateBytes();
            Console.WriteLine("Total Memory: {0}", GC.GetTotalMemory(false));
            //Optimized does not clean up, instead it cleans up if new memory gets allocated
            GC.Collect(2, GCCollectionMode.Optimized);
            Console.WriteLine("Total Memory: {0}", GC.GetTotalMemory(false));
            AllocateBytes();
            Console.WriteLine("Total Memory: {0}", GC.GetTotalMemory(false));

            //Collection Mode: Forced
            Console.WriteLine("----------------");
            Console.WriteLine("Collection Mode: Forced");
            Console.WriteLine("Total Memory: {0}", GC.GetTotalMemory(false));
            //Forced forces the garbage collector to actually clean up
            GC.Collect(2, GCCollectionMode.Forced);
            Console.WriteLine("Total Memory: {0}", GC.GetTotalMemory(false));

            Console.ReadKey();
        }



        //Generate some bytes:
        public static void AllocateBytes()
        {
            Byte[] b = new Byte[1024 * 1024 * 100];
            b = new Byte[1024 * 1024 * 1000];
            b = new Byte[1024 * 1024 * 100];
        }
    }
}
