﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace First_Program
{
    public class UnitConverter
    {
        int ratio;

        //Constructor
        public UnitConverter(int ratio)
        {
            this.ratio = ratio;
        }

        public int Convert(int unit)
        {
            return unit * ratio;
        }

    }
}
