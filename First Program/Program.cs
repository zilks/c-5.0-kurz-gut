﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace First_Program
{
    class Program
    {
        public static void Main(string[] args)
        {
            int x = 15 * 30;
            Console.WriteLine("Ergebnis: {0}", x);

            Console.WriteLine(FeetToInches(30));
            Console.WriteLine(FeetToInches(50));
            Console.WriteLine(FeetToInches(150));
            Console.WriteLine(1 + 2 + 3 + 4);

            Console.WriteLine("");

            string message = "Ahoi";
            string messageUpperCase = message.ToUpper();
            Console.WriteLine(messageUpperCase);

            int jahr = 2015;
            message = message + " " + jahr.ToString();
            Console.WriteLine(message);


            Console.WriteLine("");

            /* Booleans */
            //Booleans

            bool simpleVar = false;
            if (simpleVar)
            {
                Console.WriteLine("Wird nicht ausgegeben.");
            }
            else
            {
                Console.WriteLine("Das hier wird aber ausgegeben.");
            }


            Console.WriteLine("");

            //UnitConverter
            UnitConverter feetToInches = new UnitConverter(12);

            Console.WriteLine(feetToInches.Convert(30));


            Console.WriteLine("");


            Panda p1 = new Panda("Bernd");
            Panda p2 = new Panda("Urs");

            Console.WriteLine(p1.Name);
            Console.WriteLine(p2.Name);
            Console.WriteLine(Panda.Population);





            Console.ReadKey();
        }

        static int FeetToInches(int feet)
        {
            int inches = feet * 12;
            return inches;
        }

    }
}
